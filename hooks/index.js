export const useFormatDate = () => ({
    formatDate: date => date.toLocaleDateString()
})