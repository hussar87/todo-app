import {useContext, useState} from "react";
import {TodoContext} from "../context/TodoContext";
import {actions} from "../actions";
import {TrashIcon} from "@heroicons/react/outline";
import {useFormatDate} from "../hooks";

function TodoList() {
    const {todoList, dispatch} = useContext(TodoContext);
    const {formatDate} = useFormatDate();
    const [title, setTitle] = useState("");

    const handleChange = event => setTitle(event.target.value);
    const handleSubmit = event => {
        event.preventDefault();

        if (!title.trim()) return alert("Please enter a value");

        dispatch(
            {
                type: actions.ADD_TODO_ITEM,
                todoItem: {
                    id: Date.now(),
                    title: title,
                    createdDate: formatDate(new Date())
                }
            }
        )
        setTitle("")
    }
    const handleClear = event => {
        event.preventDefault();

        dispatch({
            type: actions.REMOVE_ALL_TODO_ITEMS
        })
    }
    const handleDelete = id => dispatch({
        type: actions.REMOVE_TODO_ITEM,
        todoItem: {
            id
        }
    })

    return (
        <div className="flex flex-col">
            <form className="flex flex-grow justify-evenly my-5" onSubmit={handleSubmit}>
                <span className="items-center uppercase">
                    <label htmlFor="todoItem">Todo</label>
                    <input className="border border-blue-300 p-1 mx-1 rounded" type="text" id="todoItem" name="todoItem"
                           onChange={handleChange} value={title}/>
                </span>
                <button
                    className="transition ease-in-out delay-150 border py-1 px-2 mx-1 rounded-2xl uppercase bg-sky-600 hover:bg-sky-700 text-white"
                    type="submit"
                >
                    add
                </button>
                <button
                    className="transition ease-in-out delay-150 border py-1 px-2 mx-1 rounded-2xl uppercase bg-red-600 hover:bg-red-700 text-white"
                    type="submit"
                    onClick={handleClear}
                >
                    remove all
                </button>
            </form>
            <div>
                {
                    todoList.map(({id, title, createdDate}) => (
                        <div
                            className="grid grid-rows-2 grid-flow-col border border-blue-300 shadow rounded-md p-4 max-w-sm w-full mx-auto mb-2"
                            key={id}
                        >
                            <h2 className="text-left text-lg underline text-sky-600">
                                {title}
                            </h2>
                            <p className="text-left text-xs text-gray-300 italic ">
                                Created: {createdDate.toString()}
                            </p>
                            <TrashIcon
                                className="row-span-2 ml-auto my-auto h-10 w-10 text-blue-500 hover:cursor-pointer hover:animate-pulse hover:text-red-500"
                                onClick={() => handleDelete(id)}
                            />
                        </div>
                    ))
                }
            </div>
        </div>
    )
}

export default TodoList