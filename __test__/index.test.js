import TodoList from "../components/TodoList";
import {render, getByTestId, waitFor} from '@testing-library/react';
import TodoProvider from "../context/TodoContext";
import {actions} from "../actions";
import {TodoReducer} from "../reducers/TodoReducer";

test('should render todoList component', () => {
    const {queryByTestId} = render(<TodoProvider><TodoList/></TodoProvider>);
    expect(queryByTestId('todoList')).toBeDefined();
})

test('should render todoList component', () => {
    render(<TodoProvider><TodoList/></TodoProvider>);
    waitFor(() => expect(getByTestId("todoList")).toBeInTheDocument());
})

test('should handle a todo item being added to an empty list', () => {
    const state = [];
    const action = {
        type: actions.ADD_TODO_ITEM,
        todoItem: {
            id: 'id',
            title: 'title',
            createdDate: 'date'
        }
    }
    expect(TodoReducer(state, action)).toEqual([
        {
            id: 'id',
            title: 'title',
            createdDate: 'date'
        }
    ])
})

test('should handle a todo item being cleared', () => {
    const state = [
        {
            id: 1,
            title: 'title1',
            createdDate: new Date()
        },
        {
            id: 2,
            title: 'title2',
            createdDate: new Date()
        },
        {
            id: 3,
            title: 'title3',
            createdDate: new Date()
        }
    ];
    const action = {
        type: actions.REMOVE_ALL_TODO_ITEMS
    }
    expect(TodoReducer(state, action)).toEqual([])
})

