import {createContext, useReducer} from "react";
import {TodoReducer} from "../reducers/TodoReducer";

export const TodoContext = createContext();

const TodoProvider = ({children}) => {
    const [todoList, dispatch] = useReducer(TodoReducer, []);

    return (
        <TodoContext.Provider value={{todoList, dispatch}}>
            {children}
        </TodoContext.Provider>
    )
}

export default TodoProvider