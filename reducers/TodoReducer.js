import {actions} from "../actions";

export const TodoReducer = (state, action) => {
    switch (action.type) {
        case actions.ADD_TODO_ITEM:
            return [...state, action.todoItem]
        case actions.REMOVE_TODO_ITEM:
            return state.filter(todoItem => todoItem.id !== action.todoItem.id)
        case actions.REMOVE_ALL_TODO_ITEMS:
            return []
        default:
            return state;
    }
}