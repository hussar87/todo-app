# TODO-APP with Next.js + Tailwind CSS & JEST

This example shows how to use [Tailwind CSS](https://tailwindcss.com/) [(v3.0)](https://tailwindcss.com/blog/tailwindcss-v3) with Next.js. and Jest for testing.

## Getting Started

This project uses node version v14.16.0
It might be useful to use [nvm](https://github.com/nvm-sh/nvm) or [nvm windows](https://github.com/coreybutler/nvm-windows)

## How to use

```
nvm use (if nvm istalled)

npm install
npm run dev
```
